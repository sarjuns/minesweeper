package CustomComponents;

import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class JPicButton extends JButton {
	int x, y, max;

	/**
	 * @param path
	 *            :"resources/graphics/index.jpg"
	 */
	public JPicButton(String fileName, int max) {
		this.max = max;
		try {
			// ImageIcon mx = new ImageIcon(JPicButton.class.getResource("/graphics/" +
			// fileName)); #works
			Image img = ImageIO.read(JPicButton.class.getResource("/graphics/" + fileName));
			img = img.getScaledInstance(10 * (40 / max), 10 * (45 / max), Image.SCALE_SMOOTH);
			ImageIcon mc = new ImageIcon(img);
			this.setIcon(mc);
		} catch (Exception ex) {
			System.out.println(ex);
			ex.printStackTrace();
		}

	}

	public JPicButton() {

	}

	public void setOrigin(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setPath(String fileName) {
		try {
			Image img = ImageIO.read(JPicButton.class.getResource("/graphics/" + fileName));
			img = img.getScaledInstance(10 * (40 / max), 10 * (45 / max), Image.SCALE_SMOOTH);
			ImageIcon mc = new ImageIcon(img);
			this.setIcon(mc);
		} catch (Exception ex) {
			System.out.println(ex);
			ex.printStackTrace();
		}
	}

	/**
	 * @return the x
	 */
	public synchronized int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public synchronized int getY() {
		return y;
	}

	/*
	 * public JPicButton(String fileName, int xMax, int yMax) { try { // ImageIcon
	 * mx = new ImageIcon(JPicButton.class.getResource("/graphics/" + // fileName));
	 * #works Image img = ImageIO.read(JPicButton.class.getResource("/graphics/" +
	 * fileName)); img = img.getScaledInstance(10 * (40 / xMax), 10 * (45 / yMax),
	 * Image.SCALE_SMOOTH); ImageIcon mc = new ImageIcon(img); this.setIcon(mc); }
	 * catch (Exception ex) { System.out.println(ex); ex.printStackTrace(); } }
	 * 
	 */
}
