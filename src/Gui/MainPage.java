package Gui;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JProgressBar;

/**
 * @author ArjunS
 *
 */
@SuppressWarnings({ "serial", "unused" })
public class MainPage extends Page {
	private JButton btnNewGame, btnContinue, btnScores, btnCredits;
	private JProgressBar progressBar;

	public MainPage() {

		setLayout(null);

		btnNewGame = new JButton("New Game");
		btnNewGame.setOpaque(false);
		btnNewGame.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 22));
		btnNewGame.setForeground(new Color(255, 0, 0));
		btnNewGame.setBounds(150, 140, 400, 40);
		add(btnNewGame);

		btnContinue = new JButton("Continue");
		btnContinue.setOpaque(false);
		btnContinue.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 22));
		btnContinue.setForeground(new Color(255, 0, 0));
		btnContinue.setBounds(150, 220, 400, 40);
		add(btnContinue);

		btnScores = new JButton("High Scores");
		btnScores.setOpaque(false);
		btnScores.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 22));
		btnScores.setForeground(new Color(255, 0, 0));
		btnScores.setBounds(150, 300, 400, 40);
		add(btnScores);

		btnCredits = new JButton("Credits");
		btnCredits.setOpaque(false);
		btnCredits.setFont(new Font("Comic Sans MS", Font.BOLD | Font.ITALIC, 22));
		btnCredits.setForeground(new Color(255, 0, 0));
		btnCredits.setBounds(150, 380, 400, 40);
		add(btnCredits);

		setVisible(true);
		setSize(700, 500);
		setBackground(new Color(0, 120, 215));

		// paintImmediately(100, 55, 500, 30);

		JLabel lblNewLabel = new JLabel("Mine Sweeper");
		lblNewLabel.setBackground(new Color(0, 153, 204));
		lblNewLabel.setOpaque(true);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Algerian", Font.BOLD, 45));
		lblNewLabel.setBounds(75, 30, 550, 80);
		add(lblNewLabel);

		progressBar = new JProgressBar(0,10);
		progressBar.setIndeterminate(true);
		progressBar.setVisible(false);
		progressBar.setBackground(Color.GREEN);
		progressBar.setOpaque(true);
		progressBar.setBounds(430, 155, 110, 10);
		add(progressBar);
		// setPenColor(new Color(0, 120, 215));
	}

	/**
	 * @return the btnContinue
	 */
	public synchronized JButton getBtnContinue() {
		return btnContinue;
	}

	/**
	 * @return the btnCredits
	 */
	public synchronized JButton getBtnCredits() {
		return btnCredits;
	}

	/**
	 * @return the btnNewGame
	 */
	public synchronized JButton getBtnNewGame() {
		return btnNewGame;
	}

	/**
	 * @return the btnScores
	 */
	public synchronized JButton getBtnScores() {
		return btnScores;
	}

	/**
	 * @return the progressBar
	 */
	public synchronized JProgressBar getProgressBar() {
		return progressBar;
	}
}
