package Gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import CustomComponents.JPicButton;

@SuppressWarnings("serial")
public class Launch extends JFrame {
	Page mainpage, gamepage;
	String playerName;

	public Launch() {

		//
		getContentPane().setLayout(null);
		setTitle("Mine Sweeper");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainPage.class.getResource("/graphics/images.jpg")));
		setResizable(false);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation((int) (dim.width / 3.5), (int) (dim.height / 7));
		setSize(705, 535);
		setVisible(true);
		setupMain();
		implementAll();

	}

	public void setupMain() {
		mainpage = new MainPage();
		mainpage.setOpaque(true);
		// getContentPane().add(mainpage);
		getLayeredPane().add(mainpage);
	}

	public void setupGame() {
		gamepage = new GamePage();
		gamepage.setOpaque(true);
		getLayeredPane().add(gamepage);
	}

	private void implementAll() {
		implimentMainActions();
		implimentGameActions();
		this.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosing(WindowEvent e) {

				int reply = JOptionPane.showConfirmDialog(null, "Are Sure to exit the game", "Confirmation Window",
						JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION) {
					// JOptionPane.showMessageDialog(null, "GOODBYE");
					setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				} else {
					setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				}

			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	private void implimentGameActions() {
		/*
		 * 		for (int y = 0; y < yMax; y++) {
			for (int x = 0; x < xMax; x++) {
				groundMatrix[x][y] = new JPicButton("Layout1d.jpg", 10);
				groundMatrix[x][y].setOrigin(x, y);
				groundMatrix[x][y].setLocation(Width * x, Width * y);
				groundMatrix[x][y].setSize(Width, Width);
				groundMatrix[x][y].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						int x = ((JPicButton) e.getSource()).getX();
						int y = ((JPicButton) e.getSource()).getY();
						groundMatrix[x][y].setPath("flag.jpg");
					}
				});
				game.add(groundMatrix[x][y]);
			}
		}
		 */

	}

	private void implimentMainActions() {

		((MainPage) mainpage).getBtnNewGame().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// playerName = JOptionPane.showInputDialog(null, "Enter your Name");
				// System.out.println(playerName);
				mainpage.setVisible(false);
				setupGame();
				gamepage.setVisible(true);
			}

		});

		((MainPage) mainpage).getBtnContinue().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		((MainPage) mainpage).getBtnScores().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		((MainPage) mainpage).getBtnCredits().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

			}
		});

	}

}
