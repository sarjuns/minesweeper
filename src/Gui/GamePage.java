package Gui;

import javax.swing.JPanel;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import CustomComponents.JPicButton;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class GamePage extends Page {
	JPicButton groundMatrix[][];

	public GamePage() {
		setBackground(Color.WHITE);
		setVisible(false);

		setLayout(null);
		setSize(700, 500);

		JLabel lblNewLabel = new JLabel("MineSweeper");
		lblNewLabel.setBounds(48, 13, 250, 50);
		add(lblNewLabel);
		lblNewLabel.setBackground(Color.PINK);
		lblNewLabel.setOpaque(true);
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 18));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);

		
		JPanel game = new JPanel();
		game.setBounds(50, 80, 600, 400);
		add(game);
		game.setLayout(null);

		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(342, 15, 66, 50);
		add(lblNewLabel_1);

		JLabel label = new JLabel("");
		label.setBounds(584, 13, 66, 50);
		add(label);

		JButton btnNewButton = new JButton("Reset");
		btnNewButton.setBounds(446, 15, 97, 50);
		add(btnNewButton);
		
		int xMax = 15, yMax = 10, Width = 40;
		groundMatrix = new JPicButton[xMax][yMax];

		for (int y = 0; y < yMax; y++) {
			for (int x = 0; x < xMax; x++) {
				groundMatrix[x][y] = new JPicButton("Layout1d.jpg", 10);
				groundMatrix[x][y].setLocation(Width * x, Width * y);
				groundMatrix[x][y].setSize(Width, Width);
				game.add(groundMatrix[x][y]);
				groundMatrix[x][y].setOrigin(x, y);
			}
		}

		// GridBagConstraints mineGround = new GridBagConstraints();
		// mineGround.gridheight = 45;
		// mineGround.gridwidth = 40;

	}

	/**
	 * @return the groundMatrix
	 */
	public synchronized JPicButton[][] getGroundMatrix() {
		return groundMatrix;
	}

	/**
	 * @param groundMatrix
	 *            the groundMatrix to set
	 */
	public synchronized void setGroundMatrix(JPicButton[][] groundMatrix) {
		this.groundMatrix = groundMatrix;
	}
}
